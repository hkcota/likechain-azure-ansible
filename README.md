# Likechain server install configuration

A recipe for install and setup Likecoin server.

## Prerequsities

### Create Application Credentials

To create application, you can use the following azure cli command:

```
az ad sp create-for-rbac --name <application name>
```

If success, you'll get the following response:

```json
{
  "appId": "some-applicaton-id",
  "displayName": "application name",
  "name": "http://application-name",
  "password": "some-application-secret",
  "tenant": "some-tenant-id"
}
```

### Find Out the Subscription ID to Use

You can use this command to search for available subscriptions in your account:

```
az account list
```

Response:

```json
[
  {
    "cloudName": "AzureCloud",
    "id": "subscription-id",
    "isDefault": true,
    "name": "The Name of the Subscription",
    "state": "Enabled",
    "tenantId": "some-tenant-id",
    "user": {
      "name": "your@email.address.com",
      "type": "user"
    }
  },

  ...

]
```

### Credential Files

Make sure that `~/.azure/credentials` is correctly setup with these settings:

```
[default]

subscription_id=<azure subscription id>
client_id=<azure client id>
secret=<secret id>
tenant=<tenant id>
```

### Setup Ansible and Dependencies

Run these commands to make sure you have everything needed to run the Ansible playbooks.

```
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirement.txt
```

## Setup Procedure


### Setup Azure with Ansible

If your credential file is setup correct, this will create a Virtual Machine in your Azure
account with all the depending resource group, routes and resources.

```
. .venv/bin/activate
ansible-playbook ./playbooks/001-CreateVirtualMachine.yml
```

At the end of all tasks, you will see a section like this:

```
TASK [Dump public IP for VM which will be created] **************************************
ok: [localhost] => {
    "msg": "The public IP is XX.XXX.XXX.XX."
}
```

The file `inventory.yml` will be generated automatically.

Please record the IP address (in place of `XX.XXX.XXX.XX`) for later use.


### Setup the Environment Variables

Although Ansible do not directly support .env format, this repository uses
a [.env file](.env.example) to store a set of variables as a reminder.

You may copy the file [.env.example](.env.example) as `.env` and then edit
all variables to your specific setup. You should setup all of the variables
before moving on to next step.

After all variables are setup (and after each time you changed it) in the
`.env` file, please run this to import all variables:

```
source .env
```

### Setup the Virtual Machine

Please remember all ansible playbook are supposed to be run in the venv context. If not, please run:

```
. .venv/bin/activate
```

To setup the virtual machine and initialize the node:

```
# Azure VM installation
for PLAYBOOK in ./playbooks/00*.yml; do ansible-playbook -i inventory.yml $PLAYBOOK || break; done

# Likechain prerequsities install and initialization
for PLAYBOOK in ./playbooks/01*.yml; do ansible-playbook -i inventory.yml $PLAYBOOK || break; done

# Setup genesis.json to server
# Download genesis.json to ./assets/genesis.json first.
sha1sum ./assets/genesis.json > ./assets/genesis.json.sha1sum

# Likechain docker-compose file and genesis.json setup
for PLAYBOOK in ./playbooks/02*.yml; do ansible-playbook -i inventory.yml $PLAYBOOK || break; done
```
